using System;
using UnityEngine;

public class DeathPlaneCheck : MonoBehaviour
{
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private float respawnDistanceFromPortal = 1.5f;
    [SerializeField] private float respawnHeight = 1.5f;

    private CharacterController characterController;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("DeathPlane"))
        {
            Transform lastPortalTransform = _playerStats.lastPortalTouched.transform;
            
            // TODO: Not currently working... Maybe due to CharacterController or PlatformHandler?
            transform.rotation = lastPortalTransform.rotation;
            transform.position = lastPortalTransform.position + Vector3.up * respawnHeight + lastPortalTransform.forward * respawnDistanceFromPortal;

            
            // Needed as the CharacterController will override teleporting
            Physics.SyncTransforms();
            
            // Character controller optional :)
            if (characterController != null)
            {
                // Behaves a bit like a jump to dampen the fall relative to height
                characterController.SimpleMove(Vector3.zero);
            }
            
        }
    }
}
