﻿using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{

    [SerializeField] private PortalRuntimeSet portalRuntimeSet;
    private List<Portal> portals = new List<Portal>();
    
    void Start () {
        portals = portalRuntimeSet.GetPortals();
    }

    void OnPreCull ()
    {
        portals = portalRuntimeSet.GetPortals();

        for (int i = 0; i < portals.Count; i++) {
            portals[i].PrePortalRender ();
        }
        for (int i = 0; i < portals.Count; i++) {
            portals[i].Render ();
        }

        for (int i = 0; i < portals.Count; i++) {
            portals[i].PostPortalRender ();
        }

    }

}