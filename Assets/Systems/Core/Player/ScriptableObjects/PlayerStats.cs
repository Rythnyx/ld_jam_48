using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PlayerStats")]
public class PlayerStats : ScriptableObject
{
    [SerializeField] [Range(0f, 10f)] public float health;

    [SerializeField] public string currentPlayerWorldLocation;
    [SerializeField] public GameObject lastPortalTouched;

}
