using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PortalsRuntimeSet")]
public class PortalRuntimeSet : RuntimeSet<Portal>
{
    public List<Portal> GetPortals()
    {
        return CurrentItems;
    }
}
