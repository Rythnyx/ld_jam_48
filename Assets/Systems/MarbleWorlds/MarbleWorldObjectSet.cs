using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/MarbleWorldObjectsRuntimeSet")]
public class MarbleWorldObjectSet : RuntimeSet<GameObject>
{
    
}
