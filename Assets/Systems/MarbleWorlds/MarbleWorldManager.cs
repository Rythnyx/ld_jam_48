using System.Collections.Generic;
using UnityEngine;

public class MarbleWorldManager : MonoBehaviour
{
    [SerializeField] private MarbleWorldRuntimeSet marbleWorldRuntimeSet;
    [SerializeField] [Range(1, 20)]private int worldIndex;
    
    private Collider marbleWorldCollider;

    private void OnEnable()
    {
        marbleWorldCollider = this.gameObject.GetComponentInChildren<MarbleSky>().GetComponent<Collider>();
        Portal[] portalsInWorld = this.gameObject.GetComponentsInChildren<Portal>();

        List<GameObject> portalGOs = new List<GameObject>();
        foreach (Portal portal in portalsInWorld)
        {
            portalGOs.Add(portal.transform.gameObject);
        }

        marbleWorldRuntimeSet.Add(this.gameObject, portalGOs, marbleWorldCollider, worldIndex);
    }

    private void OnDisable()
    {
        marbleWorldRuntimeSet.Remove(this.gameObject, marbleWorldCollider);
    }

    public int GetWorldIndex()
    {
        return worldIndex;
    }
}
