using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldPortalDisabler : MonoBehaviour
{
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private MarbleWorldRuntimeSet marbleWorldRuntimeSet;

    public void ManagePortalsStatesRelativeToPlayer()
    {
        Dictionary<string, List<GameObject>> distantPortals = marbleWorldRuntimeSet.GetPortalsOutsideLocaleRange(_playerStats.currentPlayerWorldLocation);
        Dictionary<string, List<GameObject>> nearPortals = marbleWorldRuntimeSet.GetPortalsInsideLocaleRange(_playerStats.currentPlayerWorldLocation);
        
        SetPortalActiveState(distantPortals, false);
        SetPortalActiveState(nearPortals, true);
    }

    private static void SetPortalActiveState(Dictionary<string, List<GameObject>> nearPortals, bool active)
    {
        foreach (KeyValuePair<string, List<GameObject>> portalsForWorldNames in nearPortals)
        {
            foreach (GameObject portalGo in portalsForWorldNames.Value)
            {
                // Only disable the Portal Script. Setting th GameObject to false will result in Garbage Collection
                Portal portal = portalGo.GetComponent<Portal>();
                Camera portalCam = portal.GetComponentInChildren<Camera>();
                bool portalEnabled = portal.enabled;
                if (portalEnabled != active)
                {
                    Debug.Log($"Attempting to set {portal.name} active state from {portalEnabled} to {active}");
                    portal.enabled = active;
                    portalCam.enabled = active;
                }
            }
        }
    }
}
