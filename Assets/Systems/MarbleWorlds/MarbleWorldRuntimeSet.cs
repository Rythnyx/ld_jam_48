using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/MarbleWorldRuntimeSet")]
public class MarbleWorldRuntimeSet : RuntimeSet<GameObject>
{
    [SerializeField] public MarbleWorldObjectSet marbleWorldObjectSet;
    [SerializeField] public List<Collider> marbleWorldColliders = new List<Collider>();
    [SerializeField] public Dictionary<string, List<GameObject>> marbleWorldPortalsForWorldName = new Dictionary<string, List<GameObject>>();
    
    [SerializeField] public List<GameObject> marbleWorldPortalsList = new List<GameObject>();
    private Dictionary<string, int> worldIndicesForName = new Dictionary<string, int>();

    private void OnEnable()
    {
        marbleWorldObjectSet = ScriptableObject.CreateInstance<MarbleWorldObjectSet>();
    }

    private void OnDisable()
    {
        ScriptableObject.DestroyImmediate(marbleWorldObjectSet);
    }

    public void Add(GameObject marbleWorld, List<GameObject> portals, Collider collider, int worldIndex)
    {
        // Debug.Log($"Attemting to insert {marbleWorld.name} with index {marbleWorld.GetComponent<MarbleWorldManager>().GetWorldIndex()}");
        Add(marbleWorld.name, marbleWorld);
        marbleWorldColliders.Add(collider);
        // Debug.Log($"Adding {portals.Count} portals for the world {marbleWorld.name}");
        marbleWorldPortalsForWorldName.Add(marbleWorld.name, portals);
        
        AddWorldIndices(marbleWorld);
    }

    public void Remove(GameObject marbleWorld, Collider collider)
    {
        base.Remove(marbleWorld.name);
        marbleWorldColliders.Remove(collider);
        marbleWorldPortalsForWorldName.Remove(marbleWorld.name);
        
        RemoveWorldIndices(marbleWorld);
    }

    public Dictionary<string, List<GameObject>> GetPortalsOutsideLocaleRange(string playerLocaleName)
    {
        Dictionary<string, int> nonAdjacentWorldIndicesForName = GetNonAdjacentWorldsAndIndices(playerLocaleName);
        
        Dictionary<string, List<GameObject>> worldPortals = new Dictionary<string, List<GameObject>>();
        foreach (KeyValuePair<string,int> worldIndexForName in nonAdjacentWorldIndicesForName)
        {
            // Debug.Log($"Expected Portal outside Player Range: {worldIndexForName.Key}, {worldIndexForName.Value}");
            worldPortals.Add(worldIndexForName.Key, marbleWorldPortalsForWorldName[worldIndexForName.Key]);
        }
        
        return worldPortals;
    }

    public Dictionary<string, List<GameObject>> GetPortalsInsideLocaleRange(string playerLocaleName)
    {
        Dictionary<string, int> adjacentWorldIndicesForName =  GetAdjacentWorldsAndIndices(playerLocaleName);

        Dictionary<string, List<GameObject>> worldPortals = new Dictionary<string, List<GameObject>>();
        foreach (KeyValuePair<string,int> worldIndexForName in adjacentWorldIndicesForName)
        {
            // Debug.Log($"Expected Portal inside Player Range: {worldIndexForName.Key}, {worldIndexForName.Value}");
            worldPortals.Add(worldIndexForName.Key, marbleWorldPortalsForWorldName[worldIndexForName.Key]);
        }

        return worldPortals;
    }

    private Dictionary<string, int> GetAdjacentWorldsAndIndices(string playerLocaleName)
    {
        int playerLocaleIndex = FindPlayerLocaleIndex(playerLocaleName);
      
        Dictionary<string,int> adjacentIndicesForNames = new Dictionary<string, int>();
        foreach (KeyValuePair<string,int> indicesForName in worldIndicesForName)
        {
            if (indicesForName.Value > playerLocaleIndex - 2 && indicesForName.Value < playerLocaleIndex + 2)
            {
                adjacentIndicesForNames.Add(indicesForName.Key, indicesForName.Value);
            }
        }
        
        return adjacentIndicesForNames;

    }

    private Dictionary<string,int> GetNonAdjacentWorldsAndIndices(string playerLocaleName)
    {
        int playerLocaleIndex = FindPlayerLocaleIndex(playerLocaleName);
        
        Dictionary<string,int> nonAdjacentIndicesForNames = new Dictionary<string, int>();
        foreach (KeyValuePair<string,int> indicesForName in worldIndicesForName)
        {
            if (indicesForName.Value < playerLocaleIndex - 1 || indicesForName.Value > playerLocaleIndex + 1)
            {
                nonAdjacentIndicesForNames.Add(indicesForName.Key, indicesForName.Value);
            }
        }

        return nonAdjacentIndicesForNames;
    }

    private void AddWorldIndices(GameObject world)
    {
        int worldIndex = world.GetComponent<MarbleWorldManager>().GetWorldIndex();
        worldIndicesForName.Add(world.name, worldIndex);
    }

    private void RemoveWorldIndices(GameObject world)
    {
        worldIndicesForName.Remove(world.name);
    }

    private int FindPlayerLocaleIndex(string playerLocaleName)
    {
        int playerLocale = worldIndicesForName[playerLocaleName];
        return playerLocale;
    }
}
