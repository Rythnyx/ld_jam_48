using System;
using UnityEngine;

public class WorldChecker : MonoBehaviour
{
    [SerializeField] private MarbleWorldRuntimeSet marbleWorldRuntimeSet;
    [SerializeField] private PlayerStats _playerStats;

    [SerializeField] private GameEvent WorldChangedEvent;
    
    private string previousWorldName;

    private void OnTriggerEnter(Collider other)
    {
        CheckInsideWorld(other);
    }
    
    private void OnTriggerExit(Collider other)
    {
        CheckInsideWorld(other);
    }

    public String CheckInsideWorld(Collider collider)
    {
        if (marbleWorldRuntimeSet.marbleWorldColliders.Contains(collider))
        {
            if (collider.bounds.Contains(this.gameObject.transform.position))
            {
                previousWorldName = _playerStats.currentPlayerWorldLocation;
                
                string localeName = collider.gameObject.transform.parent.name;
                _playerStats.currentPlayerWorldLocation = localeName;
                _playerStats.lastPortalTouched = collider.transform.parent.gameObject.GetComponentInChildren<Portal>().gameObject;
                
                if (localeName != previousWorldName)
                {
                    WorldChangedEvent.Raise();
                }
                
                return localeName;
            }
        }

        return "Unknown...";
    }
}
