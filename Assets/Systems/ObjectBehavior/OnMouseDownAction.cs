using UnityEngine;

public class OnMouseDownAction : MonoBehaviour
{
    [SerializeField] private GameEvent gameEvent;
    
    private void OnMouseDown()
    {
        // Debug.Log($"Player has click on {this.gameObject.name}");
        if (gameEvent != null)
        {
            gameEvent.Raise();
        }
    }
}
