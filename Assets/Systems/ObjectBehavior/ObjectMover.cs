using System.Collections;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    [SerializeField] private ObjectMoverConfig _objectMoverConfig;
    
    public Vector3 startPosition;
    public Vector3 desiredEndPosition;
    public Vector3 positionWhenStopped;

    public float timeInCurrentOscillation = 0f;
    public bool previousOscillationValue;

    private Coroutine currentOscillationCoroutine;
    private bool stopAllMovement = false;


    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        positionWhenStopped = startPosition;

        previousOscillationValue = _objectMoverConfig.oscillate;
        desiredEndPosition = positionWhenStopped + _objectMoverConfig.OscillatorMovementVector;
    }

    // Update is called once per frame
    void Update()
    {
        if (stopAllMovement)
        {
            return;
        }
        
        // OscillateObject();
        OscillateObjectTimeLinked();

        RotateObject();
    }

    public void StopStartObjectMovement()
    {
        stopAllMovement = !stopAllMovement;
    }
    

    private void OscillateObjectTimeLinked()
    {
        if (_objectMoverConfig.oscillate)
        {
            if (_objectMoverConfig.oscillatorPeriod <= Mathf.Epsilon)
            {
                return;
            }

            float cycles = Time.time / _objectMoverConfig.oscillatorPeriod;

            const float tau = Mathf.PI * 2;
            float rawSinWave = Mathf.Sin(cycles * tau);

            float movementFactor = (rawSinWave + 1f) / 2;

            Vector3 offset = _objectMoverConfig.OscillatorMovementVector * movementFactor;
            transform.position = startPosition + offset;
        }
    }

    private void RotateObject()
    {
        if (_objectMoverConfig.rotate)
        {
            if (_objectMoverConfig.rotationSpeed <= Mathf.Epsilon)
            {
                return;
            }

            const float tau = Mathf.PI * 2;

            float rotationAmountPerTime = (2 * tau * Time.deltaTime * _objectMoverConfig.rotationSpeed);

            Vector3 rotation = _objectMoverConfig.rotateMovementVector * rotationAmountPerTime;

            transform.Rotate(rotation);
        }
    }


    // TODO: This code mostly works but needs tweaking to reconcile some of the issues
    // with tweaking the oscillation movement vector during runtime
    private void OscillateObject()
    {
        if (_objectMoverConfig.oscillate)
        {
            StartOscillatingObject();
        } else if (previousOscillationValue == true && previousOscillationValue != _objectMoverConfig.oscillate)
        {
            StopOscillatingObject();
        }
    }

    private void StartOscillatingObject()
    {
        if (currentOscillationCoroutine == null)
        {
            currentOscillationCoroutine = StartCoroutine(OscillateOverTime());
            previousOscillationValue = true;
        }
    }

    private void StopOscillatingObject()
    {
        StopCoroutine(currentOscillationCoroutine);
        currentOscillationCoroutine = null;

        previousOscillationValue = false;
        CalculateInterruptedOscillationValues();
    }

    private IEnumerator OscillateOverTime()
    {
        while (timeInCurrentOscillation < _objectMoverConfig.oscillatorPeriod)
        {
            float oscillatorPeriodSlice = timeInCurrentOscillation / _objectMoverConfig.oscillatorPeriod;
            transform.position = Vector3.Lerp(positionWhenStopped, desiredEndPosition,
                oscillatorPeriodSlice);   
            
            timeInCurrentOscillation += Time.deltaTime;
    #if UNITY_EDITOR
            yield return null;
    #else
            yield return new WaitForEndOfFrame();
    #endif
        }

        transform.position = desiredEndPosition;
        positionWhenStopped = desiredEndPosition;

        Vector3 oscillatorMovementVector = _objectMoverConfig.OscillatorMovementVector;
        desiredEndPosition = startPosition == desiredEndPosition ?
            startPosition + oscillatorMovementVector : positionWhenStopped - oscillatorMovementVector;
        
        timeInCurrentOscillation = 0f;
        currentOscillationCoroutine = StartCoroutine(OscillateOverTime()); // Remove this... shouldn't need recursive calls
        yield return null;
    }

    private void CalculateInterruptedOscillationValues()
    {
        positionWhenStopped = transform.position;
    }
}
