using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ObjectMoverConfig")]
public class ObjectMoverConfig : ScriptableObject
{
    [SerializeField] public bool oscillate = false;
    [SerializeField] [Range(0.1f, 10f)] public float oscillatorPeriod = 5f;
    [SerializeField] public Vector3 OscillatorMovementVector = new Vector3(0,0,0);

    [SerializeField] public bool rotate = false;
    [SerializeField] [Range(0.1f, 20f)] public float rotationSpeed = 5f;
    [SerializeField] public Vector3 rotateMovementVector = new Vector3(0,0,0);
}
