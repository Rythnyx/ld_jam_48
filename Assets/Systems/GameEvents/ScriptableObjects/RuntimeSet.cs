using System.Collections.Generic;
using UnityEngine;

public abstract class RuntimeSet<T> : ScriptableObject
{
    [SerializeField] private Dictionary<string, T> Items = new Dictionary<string, T>();
    [SerializeField] protected List<T> CurrentItems = new List<T>();

    public bool Add(string key, T value)
    {
        if (Items.ContainsKey(key))
            return false;
        
        Items.Add(key, value);
        CurrentItems.Add(value);
        return true;
    }

    public virtual void Remove(string key)
    {
        if (Items.ContainsKey(key))
        {
            CurrentItems.Remove(Items[key]);
            Items.Remove(key);
        }

    }
}
