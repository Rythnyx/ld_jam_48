using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventRaiser : MonoBehaviour
{
    [SerializeField] private GameEvent Event;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown (KeyCode.J)) {
            Event.Raise();
        }
    }
}
