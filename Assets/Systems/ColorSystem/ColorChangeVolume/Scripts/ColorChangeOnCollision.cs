using System.Collections;
using UnityEngine;

public class ColorChangeOnCollision : MonoBehaviour
{
    [SerializeField] private ColorChangeData colorChangeData;
    
    private Material material;
    private bool colorIsChanging = false;

    private void Start()
    {
        MeshRenderer meshRenderer = this.GetComponent<MeshRenderer>();
        material = meshRenderer.material;

        if (colorChangeData == null)
            colorChangeData = ScriptableObject.CreateInstance<ColorChangeData>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!colorIsChanging && other.gameObject.CompareTag("ColorChangeEntity"))
        {
            colorIsChanging = true;
            StartCoroutine(LerpColorForGameObjectMaterial());
        }
    }

    public IEnumerator LerpColorForGameObjectMaterial()
    {
        Color startColor = material.color;
        
        do
        {
            foreach (Color color in colorChangeData.colors)
            {
                float currentTime = 0f;
                while (currentTime < colorChangeData.timeToLerpBetweenColors)
                {
                    material.color = Color.Lerp(startColor, color, currentTime / colorChangeData.timeToLerpBetweenColors);
                    currentTime += Time.deltaTime;
                
    #if UNITY_EDITOR
                yield return null;
    #else
                yield return new WaitForEndOfFrame();
    #endif
                }
                
                material.color = color;
                startColor = color;
            }
        } while (colorChangeData.loop);
    }
}
