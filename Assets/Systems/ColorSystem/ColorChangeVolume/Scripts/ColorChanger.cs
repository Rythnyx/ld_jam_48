using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{ 
    [SerializeField] private ColorChangeData colorChangeData;
    
    private Material material;
    private bool colorIsChanging = false;
    private bool isOriginalColor = true;

    private Color originalColor;

    private void Start()
    {
        MeshRenderer meshRenderer = this.GetComponent<MeshRenderer>();
        material = meshRenderer.material;
        originalColor = material.color;

        if (colorChangeData == null)
            colorChangeData = ScriptableObject.CreateInstance<ColorChangeData>();
    }

    public void ChangeObjectColor()
    {
        if (!colorIsChanging)
        {
            colorIsChanging = true;
            StartCoroutine(LerpColorForGameObjectMaterial());
        }
    }
    
    private IEnumerator LerpColorForGameObjectMaterial()
    {
        Color startColor = material.color;
        
        do
        {
            // if in a loop we always want to circle back to first color
            // if not looping, then we want to check if the current color is the original and iterate through the colors backwards
            // (We explicitly have to check the loop is not enabled otherwise we can get into funky states since we invert the isOriginalColor bool)
            if (colorChangeData.loop || (isOriginalColor && !colorChangeData.loop))
            {
                foreach (Color color in colorChangeData.colors)
                {
                    float currentTime = 0f;
                    while (currentTime < colorChangeData.timeToLerpBetweenColors)
                    {
                        material.color = Color.Lerp(startColor, color,
                            currentTime / colorChangeData.timeToLerpBetweenColors);
                        currentTime += Time.deltaTime;

#if UNITY_EDITOR
                        yield return null;
#else
                yield return new WaitForEndOfFrame();
#endif
                    }

                    material.color = color;
                    startColor = color;
                }
            }
            else
            {
                // We need a copy of the list that we can reverse and add the original color to
                List<Color> colors = new List<Color>(colorChangeData.colors);
                colors.Reverse();
                colors.Add(originalColor);
                
                foreach (Color color in colors)
                {
                    float currentTime = 0f;
                    while (currentTime < colorChangeData.timeToLerpBetweenColors)
                    {
                        material.color = Color.Lerp(startColor, color,
                            currentTime / colorChangeData.timeToLerpBetweenColors);
                        currentTime += Time.deltaTime;

#if UNITY_EDITOR
                        yield return null;
#else
                yield return new WaitForEndOfFrame();
#endif
                    }

                    material.color = color;
                    startColor = color;
                }
            }
        } while (colorChangeData.loop);

        isOriginalColor = !isOriginalColor; //invert so we can iterate backwards when we need to
        colorIsChanging = false;
    }
}
