using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ColorChangeDataObject")]
public class ColorChangeData : ScriptableObject
{
    public List<Color> colors = new List<Color>() { Color.red, Color.blue, Color.green};
    [Range(0.00001f, 5f)] public float timeToLerpBetweenColors = 1.25f;
    public bool loop = false;
}
