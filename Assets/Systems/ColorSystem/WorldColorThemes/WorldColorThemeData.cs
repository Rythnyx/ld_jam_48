using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/WorldColorTheme")]
public class WorldColorThemeData : ScriptableObject
{
    [Header("World Marble Colors")]
    [SerializeField] public Color SkyTopTone;
    [SerializeField] public Color SkyMidTone;
    [SerializeField] public Color SkyBottomTone;

    [Header("Tree Colors")] 
    [SerializeField] public Color TreeTrunkColor;
    [SerializeField] public Color TreeLeavesColor;

    [Header("Rock Values")]
    [SerializeField] public Color RockColor;
    [SerializeField] [Range(0, 1f)] public float smoothness;

}
