using UnityEngine;
using TMPro;

public class PlayerStatsUIUpdater : MonoBehaviour
{
    [SerializeField] private PlayerStats _playerStats;
    
    private TextMeshProUGUI debugUI;

    private bool debugEnabled = false;
    
    
    private const string EMPTY_STRING = "";

    void Start()
    {
        debugUI = this.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            debugEnabled = !debugEnabled;
            if (!debugEnabled)
                ClearDebug();
        }
        
        if (debugEnabled)
        {
            DrawDebugStats();
        }

        DrawNormalUI();
    }

    private void DrawNormalUI()
    {
        return;
    }

    private void DrawDebugStats()
    {
        debugUI.text = _playerStats.currentPlayerWorldLocation;
    }

    private void ClearDebug()
    {
        debugUI.text = EMPTY_STRING;
    }
}
