# Ludum Dare 48 - Marble Hopper
This is a game created in 72 hours for the theme of Deeper and Deeper.

I really wanted to try playing with portals and building little worlds.
Even if it was to the dismay of adhearing well to the theme.

It's missing sound and is very short, but the systems created allow for quick iteration to create new worlds.
The game is hosted on itch here: https://rythnyx.itch.io/marble-hopper

Huge credits to Sebastion Lague whose Coding Adventure project on portals was what I started this game's core off of.
Link to that github repo here: https://github.com/SebLague/Portals
